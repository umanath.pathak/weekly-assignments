```
Deploy SpringHybernateApp to Tomcat server using Jenkins 
```
```
Pre-requisites:-
```
1. Tomcat should be installed (I have used Version 9) which is running on port 9090 on my machine.
2. Tomcat user should have proper password attached.
3. Tomcat user should have "manager-gui" and "manager-script" roles attached in 'tomcat-users.xml' file which is located inside '/opt/tomcat/conf/' directory.

```
Steps to perform:-
```
1. Login to Jenkins server through browser.

2. Select 'New' then select 'Pipeline' job. (Here 3rd option)
![Images](Images/A.png)

3. Goto Pipeline section and paste your script (I have used declarative script)
![Images](Images/1.png)

4. Build your job.

5. Console Output for git repo clone.
![Images](Images/2-Git-Repo-Clone.png)

6. Console Output for mvn clean.
![Images](Images/3-mvn-clean.png)

7. Console Output for mvn validate.
![Images](Images/4-mvn-validate.png)

8. Console Output for mvn package.
![Images](Images/5-mvn-package-1.png)

9. Console Output for checkstyle:checkstyle.
![Images](Images/6-mvn-checkstyle:checkstyle.png)

10. Console Output for findbugs:findbugs.
![Images](Images/7-mvn-findbugs-findbugs.png)

11. Console Output for cobertura:cobertura.
![Images](Images/8-mvn-cobertura-cobertura.png)

12. Console Output for mvn deploy.
![Images](Images/9-Deploy.png)

13. Screenshot for previous Build Result.
![Images](Images/10-Build-Result.png)

14. Screenshot of SpringHybernate app deployed at Tomcat server.
![Images](Images/11-Deployed_at_Tomcat_Server.png)

15. Screenshot of Job Workspace.
![Images](Images/12-Workspace.png)

16. Contents of Taget Folder, Also Checkstyle report generated.
![Images](Images/13-Target_and_Checkstyle.png)

17. Findbugs report generated.
![Images](Images/14-findbugs.png)

18. Cobertura report generated.
![Images](Images/15-Cobertura.png)

